# Armanator

Discord bot for informing users about Arma 3 servers. This bot shows server name, 
player count and mission and map info.

## How to use

**1.** Go into the directory where you want the bot directory to reside, e.g.:  
 ```cd ~```

**2.** Clone the repo:  
 ```git clone https://gitlab.com/ArmaOnUnix/Armanator.git```

**3.** Now you need to manually create/copy here **armanator_token.key** and **armanator.json**. You
 can use **armanator_token_example.key** and **armanator_example.json** as reference.

**armanator_token.key** file contains 2 strings. The first string is your bot token and the second 
one is the channel ID where the server info is shown and automatically refreshed via the 
*refresh_server_info()* function. You can use *armanator_token_example.key* file as refrence. 
Below you can see an example of the token.key file content:
```json
{
    "client_id": "595380168260884516",
    "secret": "NTI1Mn3iMTY2MjYwODgzNTE2-Dv-oHw.LBQNlnN78hFAVlAkXirOaVyfSx4"
}
```

**armanator.json** contains a list of your servers and bot settings. Below you can see an example 
of the *servers.conf* file content:
```json
{
    // links to important community webpages
    "links": [
      "**Web page**: https://armaonunix.gitlab.io/",
      "**Discord**: https://discord.gg/p28Ra36",
      "**Steam group**: https://steamcommunity.com/groups/ArmaOnUnix",
      "**Reddit**: https://www.reddit.com/r/armaonunix/",
      "**Email**: armaonunix@gmail.com",
      "**Gitlab**: https://gitlab.com/groups/ArmaOnUnix"
    ],
    // arma game servers
    "game_servers":[
      {
        "host": "armaonunix.duckdns.org",
        "query_port": 2303,
        "port": 2302
      },
      {
        "host": "armaonunix.duckdns.org",
        "query_port": 2323,
        "port": 2322
      }
    ],
    // discord server name
    "server_name": "test_server",
    // discord server ID
    "server_id": "387370249323937802",
    // discord channel that is used for bot interaction
    "bot_channel_id": "387370249323937804",
    // discord channel for showing server info (server status with player info)
    "server_info_channel_id": "533939161233752065",
    // refresh interval for server info
    "server_info_refresh_secs": 60,
    // name of the admin role (needed for commands like restart and shutdown)
    "role_admin": "Admin",
    // use embeds instead of classic text (visually more appealing)
    "embed": true,
    // database file name
    "db_filename": "armanator.db",
    // interval for pushing new data into database
    "db_info_refresh": 5
}
```
**things_to_play_with.txt** contains items used in change_game(). It is a list of games the bot is 
playing (with).

**4.** Build the bot docker image by running:

```bash build_armanator.sh```.

**5.** When the docker image is ready, you can create a docker container and run the bot:

```bash run_armanator.sh```

**6.** Enjoy !


## Discord commands
  **!help**    - Shows help.  
  **!8ball**   - Random answer.  
  **!server**  - Shows server info.  
  **!players** - Shows player info.  
  **!all**     - Shows player and server info.  
  **!info**    - Shows important community URLs.  
  **!play**    - Shows server connection info.  
  **!shutdown**- Turns off the Armanator bot. (Admin only)  
  **!restart** - Restarts the Armanator bot. (Admin only)  
  **!update**  - Updates bot from the repository. (Admin only)  
  **!stats**   - Shows basic Discord server stats.  