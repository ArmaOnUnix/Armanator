# This file contains database which stores mission-related server data
from datetime import datetime
import sys
from sqlalchemy import Column, ForeignKey, Integer, String, Float, engine, desc, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import OperationalError

Base = declarative_base()


class Server(Base):
    __tablename__ = "server"
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)


class Mission(Base):
    __tablename__ = "mission"
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)


class MissionDate(Base):
    __tablename__ = "mission_date"
    id = Column(Integer, primary_key=True)
    server_id = Column(Integer, ForeignKey("server.id"))
    mission_id = Column(Integer, ForeignKey("mission.id"))
    timestamp = Column(String, nullable=False)
    playtime = Column(Float)
    player_minute = Column(Float)


class Player(Base):
    __tablename__ = "player"
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)


class PlayerMS(Base):
    """Player Mission Server table."""
    __tablename__ = "player_mission"
    id = Column(Integer, primary_key=True)
    player_id = Column(Integer, ForeignKey("player.id"))
    mission_id = Column(Integer, ForeignKey("mission.id"))
    server_id = Column(Integer, ForeignKey("mission.id"))
    score = Column(Integer)
    # playtime in minutes
    playtime = Column(Float)


class Database:
    def __init__(self, config, logger, db_file_name=""):
        self.logger = logger.getChild("Database")
        self.config = config
        self.load_db(db_file_name)

    def load_db(self, db_file_name):
        try:
            if not db_file_name:
                file_name = "sqlite:///" + self.config.db_file_name
            else:
                file_name = "sqlite:///" + db_file_name
            self.engine = create_engine(file_name)
            self.create_tables()
            # Bind the engine to the metadata of the Base class so that the
            # declaratives can be accessed through a DBSession instance
            Base.metadata.bind = self.engine
            DBSession = sessionmaker(bind=self.engine)
            # DBSession.configure(bind=engine)
            # A DBSession() instance establishes all conversations with the database and represents
            # a "staging zone" for all the objects loaded into the database session object.
            self.session = DBSession()
        except Exception as e:
            self.logger.getChild("load_db").error(f"{e}")
            sys.exit(1)

    def close_db(self):
        self.logger.getChild("close_db").info("Closing the database...")
        self.session.close()

    def create_tables(self):
        Base.metadata.create_all(self.engine)

    def trim(self, s: str):
        s = s.replace("\"", "")
        s = s.replace("\"", "")
        return s

    def get_player_id(self, player):
        """Select player id bind to player name, if it does not exists, create it first."""
        temp_player = self.session.query(Player).filter(
            Player.name == player["name"]).first()
        # create new Player if it does not exist
        if temp_player is None:
            new_player = Player(name=player["name"])
            self.session.add(new_player)
            self.session.commit()
            temp_player = self.session.query(Player).filter(
                Player.name == player["name"]).first()
        return temp_player.id

    def get_mission_id(self, mission_name):
        """Select player id bind to mission name, if it does not exists, create it first."""
        temp_mission = self.session.query(Mission).filter(Mission.name == mission_name).first()
        if temp_mission is None:
            new_mission = Mission(name=mission_name)
            self.session.add(new_mission)
            self.session.commit()
            temp_mission = self.session.query(Mission).filter(Mission.name == mission_name).first()
        return temp_mission.id

    def get_server_id(self, server_name):
        """Select player id bind to server name, if it does not exists, create it first."""
        temp_server = self.session.query(Server).filter(Server.name == server_name).first()
        if temp_server is None:
            new_server = Server(name=server_name)
            self.session.add(new_server)
            self.session.commit()
            temp_server = self.session.query(Server).filter(Server.name == server_name).first()
        return temp_server.id

    def update_mission_date(self, server_id: int, mission_id: int, no_players: int,
                            db_info_refresh: int):
        """Create a new item if the mission_date for mission_id and todays date does not exist,
        otherwise update the playtime and player_minute mission score."""
        timestamp = datetime.utcnow().date().strftime("%y-%m-%d")
        temp_md = self.session.query(MissionDate).filter(
            MissionDate.mission_id == mission_id,
            MissionDate.server_id == server_id,
            MissionDate.timestamp == timestamp
        ).first()

        if temp_md is None:
            new_mission_date = MissionDate(
                server_id=server_id, mission_id=mission_id, timestamp=timestamp, playtime=0,
                player_minute=0
            )
            self.session.add(new_mission_date)
            self.session.commit()
        else:
            result_id = temp_md.id
            md = self.session.query(MissionDate).filter(MissionDate.id == result_id).first()
            md.playtime += self.config.db_info_refresh
            md.player_minute += self.config.db_info_refresh * no_players
            self.session.commit()

    def update_plms(self, player, player_id, server_id, mission_id, previous_players,
                    db_info_refresh):
        """Create a new item if the player mission server (plms) item for the input data does not
        exist, otherwise update the plms item."""
        temp_plms = self.session.query(PlayerMS).filter(PlayerMS.player_id == player_id,
                                                        PlayerMS.mission_id == mission_id,
                                                        PlayerMS.server_id == server_id).first()
        # if the PlayerMS does not exist, create a new one
        if temp_plms is None:
            new_plms = PlayerMS(player_id=player_id, mission_id=mission_id,
                                server_id=server_id, score=player["score"],
                                playtime=int(player["duration"] / 60))
            self.session.add(new_plms)
            self.session.commit()
        else:
            previous_player = self.find_player_name(previous_players, player["name"])

            # player just connected, no previous data
            if previous_player is None:
                # duration is in seconds
                temp_plms.playtime += int(player["duration"] / 60)
                return
            # if player got kicked, or had to reconnect -> points from last save are lost
            elif player["score"] == 0 and \
                    abs(previous_player["score"]) > self.config.score_threshold:
                return
            else:
                temp_plms.score += player["score"] - previous_player["score"]
                # db_info_refresh is in minutes
                temp_plms.playtime += db_info_refresh
                self.session.commit()

    def push_data(self, server_name: str, mission_name: str, no_players: int, players: list,
                  previous_players: list, db_info_refresh: int):
        server_name = self.trim(server_name)
        mission_name = self.trim(mission_name)

        try:
            server_id = self.get_server_id(server_name)
            mission_id = self.get_mission_id(mission_name)
            self.update_mission_date(server_id, mission_id, no_players, db_info_refresh)

            for player in players:
                player_id = self.get_player_id(player)

                # PlayerMS
                self.update_plms(player, player_id, server_id, mission_id, previous_players,
                                 db_info_refresh)
        except Exception as e:
            self.logger.getChild("push_data").error(f"{e}")

    def find_player_name(self, players, name: str):
        for player in players:
            if player["name"] == name:
                return player

    def get_stats_server(self, server_name: str, limit=5):
        try:
            server_name = self.trim(server_name)
            timestamp = datetime.utcnow().date().strftime("%y-%m")

            # name, playtime, player_minute / playtime
            result = self.session.query(Mission.name, func.sum(MissionDate.playtime),
                                        func.sum(MissionDate.player_minute)
                                        / func.sum(MissionDate.playtime)) \
                .filter(Mission.id == MissionDate.mission_id) \
                .filter(Server.id == MissionDate.server_id) \
                .filter(Server.name == server_name) \
                .filter(MissionDate.timestamp.like(timestamp + "%")) \
                .group_by(Mission.name) \
                .order_by(desc(func.sum(MissionDate.playtime))) \
                .limit(limit).all()
            return result
        except Exception as e:
            self.logger.getChild("get_stats").error(f"{e}")

    def get_stats_player(self, server_name: str, player_name: str, limit=5):
        try:
            server_name = self.trim(server_name)
            # playtime in hours
            total_sc = self.session.query(func.sum(PlayerMS.score), func.sum(PlayerMS.playtime)/60.0) \
                .filter(PlayerMS.player_id == Player.id) \
                .filter(PlayerMS.server_id == Server.id) \
                .filter(Server.name == server_name) \
                .filter(func.lower(Player.name) == player_name.lower()).first()

            # name, playtime, player_minute / playtime
            missions_sc = self.session.query(Mission.name, PlayerMS.score, PlayerMS.playtime/60.0) \
                .filter(PlayerMS.server_id == Server.id) \
                .filter(PlayerMS.mission_id == Mission.id) \
                .filter(PlayerMS.player_id == Player.id) \
                .filter(Server.name == server_name) \
                .filter(func.lower(Player.name) == player_name.lower()) \
                .order_by(desc(PlayerMS.score)) \
                .order_by(desc(PlayerMS.playtime)) \
                .limit(limit).all()

            if total_sc is None or missions_sc is None:
                return None, None
            else:
                return total_sc, missions_sc
        except Exception as e:
            self.logger.getChild("get_stats_player").error(f"{e}")
