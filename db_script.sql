/*

    user
    - steam_id      TEXT        PRIMARY KEY
    - name          TEXT


    mission
    - mission_id    INTEGER     PRIMARY KEY
    - name          TEXT


    mission_data
    - md_id
    - mission_id    INTEGER     FOREING KEY
    - duration_played
    - month         INTEGER

*/