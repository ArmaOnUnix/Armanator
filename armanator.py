import asyncio
from discord import Color, Guild, Embed, Game, TextChannel
from discord.ext import commands
import os
import random
import sys

from common import *
from config import Config
from database import Database


running = True

logger = logger.getChild("armanator")
config = Config(logger)
config.load_conf()
config.load_games()
config.load_tokens()

db = Database(config, logger)
armanator = commands.Bot(command_prefix=config.bot_prefix)


async def close(exit_code=0):
    global running
    running = False
    await armanator.close()
    # await asyncio.get_event_loop().stop()
    sys.exit(exit_code)


async def delete_messages(channel, limit):
    """Remove old messages on the discord channel."""
    try:
        msgs = await channel.history(limit=limit).flatten()
        await channel.delete_messages(msgs)
        return True
    except Exception as e:
        logger.getChild("delete_messages").error(f"{e}")


async def check_restart():
    """Checks if the bot has been restarted or updated."""
    logger.getChild("check_restart").info(f"args: {str(sys.argv)}")
    if len(sys.argv) == 3:
        if sys.argv[2] == "0":
            await config.bot_channel.send("Updated succesfully :ballot_box_with_check:")
            logger.getChild("check_restart").info("Updated successfully.")
        else:
            await config.bot_channel.send("Update failed :poop:")
            logger.getChild("check_restart").warning("Update failed.")
    # if this is a restarted instance -> inform user about the restart
    elif len(sys.argv) == 2 and sys.argv[1] == "restart":
        await config.bot_channel.send("Restarted :ballot_box_with_check:")
        logger.getChild("check_restart").info("Restarted.")


async def dialog_yn(cmd_name: str, question: str, author, channel, clb, admin_needed=False):
    """Discord dialog with yes/no answer."""
    if not admin_needed or config.role_admin in author.roles:
        await channel.send(f"{question} [y/n] - {author.mention}")
        logger.debug(f"{question} [y/n] - {author.mention}")

        def check(msg):
            return msg.author == author and msg.channel == channel

        msg = await armanator.wait_for("message", check=check)
        if msg:
            if msg.content.lower() == "y" or msg.content.lower() == "yes":
                await channel.send(f"{cmd_name} in progress...")
                logger.debug(f"{cmd_name} in progress...")
                await asyncio.sleep(3)
                # calls callback
                await clb()
            elif msg.content.lower() == "n" or msg.content.lower() == "no":
                await channel.send(f"{cmd_name} cancelled.")
                logger.debug(f"{cmd_name} cancelled.")
            else:
                await channel.send("User didn't enter a valid option.")
                logger.debug("User didn't enter a valid option.")
    else:
        msg = f"You are not allowed to use {cmd_name} command! (Admin only command)"
        await channel.send(msg)
        logger.debug("{msg} - {author}")


async def restart_program(update_result=None):
    """Restarts the bot."""
    try:
        await asyncio.sleep(3)
        # in case of update restart
        if update_result is not None:
            # works only in the Python docker, otherwise /usr/bin/python3 should be user as execl file
            os.execl("/usr/local/bin/python3", "python3", "armanator.py", "restart",
                     f"{update_result}")
        else:
            os.execl("/usr/local/bin/python3", "python3", "armanator.py", "restart")
    except OSError as e:
        logger.getChild("restart_program").error(f"{e}")


async def update_program():
    result = os.system("git pull")
    logger.info(result)
    await restart_program(result)


async def send_msg(msg_content, channel, add_separator=False):
    if config.embed:
        await channel.send(embed=msg_content)
    else:
        if add_separator:
            msg_content += "\n" + config.separator
        await channel.send(msg_content)


async def edit_msg(msg, new_msg_content, add_separator=False):
    if config.embed:
        await msg.edit(embed=new_msg_content)
    else:
        if add_separator:
            new_msg_content += "\n" + config.separator
        await msg.edit(new_msg_content)


# ~~~~~ loops ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# async def change_bot_activity():
    """Change game name the bot is playing."""
    while running:
        try:
            await armanator.change_presence(
                activity=Game(name="with " + random.choice(config.games)))
            await asyncio.sleep(random.randint(config.change_game_interval_min,
                                               config.change_game_interval_max))
        except Exception as e:
            logger.getChild("change_game").error(f"{e}")


async def change_image():
    while running:
        try:
            base_url = (
                "https://gitlab.com/ArmaOnUnix/Armanator/raw/master/img/<number>.jpg?inline=false")
            no = str(random.randint(1, 9))
            if len(no) == 1:
                no = "0" + no
            config.current_image_url = base_url.replace("<number>", no)
            await asyncio.sleep(random.randint(100, 3600))
        except Exception as e:
            logger.getChild("change_image").error(f"{e}")


async def refresh_db():
    """Loop for updating DB info like players, """
    while running:
        try:
            # if get_info_db its a legit output, e.g. no servers are populated
            data = await get_info_db(config.game_servers)
            for row in data:
                db.push_data(row[0], row[1], row[2], row[3], row[4], config.db_info_refresh)
            await asyncio.sleep(config.db_info_refresh * 60)
        except Exception as e:
            logger.getChild("refresh_db").error(f"{e}")


async def refresh_server_info():
    """Refreshes server info for server info channel every x seconds."""
    channel = config.server_info_channel
    await retry_async(delete_messages, channel, 1000, retry_count=config.retry_count)
    edit = False
    retry_failed = {}

    # server_info refresh loop
    while running:
        try:
            for i in range(len(config.game_servers)):
                address = config.game_servers[i]

                # prepare the message
                result = await get_info(address, server_info=True, player_info=True,
                                        embed=config.embed, timestamp_info=True)
                address_key = address['host'] + str(address['port'])

                # if could not get server data, add 1 to the retry_failed counter
                if not result:
                    if address_key not in retry_failed:
                        retry_failed[address_key] = 1
                        if not edit:
                            result = get_offline_msg(address, config.embed, True)
                    else:
                        retry_failed[address_key] += 1
                        if retry_failed[address_key] >= config.game_server_refresh_count:
                            result = get_offline_msg(address, config.embed, True)
                        else:
                            logger.getChild("!refresh_server_info").warning(
                                "get_info returned None")
                            continue
                else:
                    retry_failed[address_key] = 0

                # write first message
                if not edit:
                    if i < len(config.game_servers) - 1:
                        await send_msg(result, channel, True)
                    else:
                        await send_msg(result, channel, False)
                # edit existing message
                else:
                    # get all the messages
                    msgs = await channel.history(limit=100, oldest_first=True).flatten()
                    if i < len(config.game_servers) - 1:
                        await edit_msg(msgs[i], result, True)
                    else:
                        await edit_msg(msgs[i], result, False)
            # refresh every config.server_info_refresh_secs seconds
            await asyncio.sleep(config.server_info_refresh_secs)
        except Exception as e:
            logger.getChild("refresh_server_info").error(f"{e}")
            # in case something went wrong, remove all the messages and start again
            try:
                await retry_async(delete_messages, channel, 1000, retry_count=config.retry_count)
                edit = False
            except Exception as e:
                logger.getChild("refresh_server_info").error(f"delete_messages {e}")
        else:
            edit = True


# ~~~~ events ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@armanator.event
async def on_ready():
    logger.getChild("on_ready").info("Logged in as")
    logger.getChild("on_ready").info(armanator.user.name)
    logger.getChild("on_ready").info(armanator.user.id)
    logger.getChild("on_ready").info(config.separator)

    # find the right discord server
    for server in armanator.guilds:
        if server.id == config.server_id or server.name == config.server_name:
            config.discord_server = server
            break

    # find the bot channel
    config.bot_channel = armanator.get_channel(config.bot_channel_id)
    config.server_info_channel = armanator.get_channel(config.server_info_channel_id)

    # find the admin role
    for role in config.discord_server.roles:
        if role.name == config.role_admin_name:
            config.role_admin = role

    # checks if the bot has been restarted or updated.
    await check_restart()

    # clean #server_info channel
    await delete_messages(config.server_info_channel, 1000)

    armanator.loop.create_task(change_bot_activity())
    armanator.loop.create_task(change_image())
    armanator.loop.create_task(refresh_server_info())
    armanator.loop.create_task(refresh_db())


# ~~~~~ commands ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@armanator.command(name="8ball",
                   description="Wisdom from a certified* oracle.",
                   brief="Random answer.",
                   pass_context=True)
async def eight_ball(ctx):
    """8ball, because why not..."""
    try:
        answers = [
            "Nope",
            "Probably not",
            "I am not sure",
            "Maybe",
            "I guess so",
            "Definitely",
            "... and what do you think ?",
            "Good question",
            "No comment"
        ]
        await config.bot_channel.send(random.choice(answers))
    except Exception as e:
        logger.getChild("eight_ball").error(f"{e}")

# DELETE as it's unnecessary, you can look this info up in the #server_info channel
# @armanator.command(name="server",
#                    description=("Shows information about the server" +
#                                 "(mission, maps, no of players)."),
#                    brief="Shows server info.",
#                    aliases=["servers"],
#                    pass_context=True)
# async def server(ctx):
#     """Shows information about Valve server (mission, maps, no of players)."""
#     try:
#         for i in range(len(config.game_servers)):
#             address = config.game_servers[i]
#             result = await get_info(address, server_info=True, embed=config.embed)
#             if result is None:
#                 raise Exception("get_info returned None")
#
#             if i < len(config.game_servers) - 1:
#                 await send_msg(result, config.bot_channel, True)
#             else:
#                 await send_msg(result, config.bot_channel, False)
#     except Exception as e:
#         logger.getChild("!server").error(f"{e}")
#
#
# @armanator.command(name="players",
#                    description="Shows players on the server.",
#                    brief="Shows player info.",
#                    aliases=["player"],
#                    pass_context=True)
# async def players(ctx):
#     """Shows information about players."""
#     try:
#         for i in range(len(config.game_servers)):
#             address = config.game_servers[i]
#             result = await get_info(address, player_info=True, embed=config.embed)
#             if result is None:
#                 raise Exception("get_info returned None")
#
#             if i < len(config.game_servers) - 1:
#                 await send_msg(result, config.bot_channel, True)
#             else:
#                 await send_msg(result, config.bot_channel, False)
#     except Exception as e:
#         logger.getChild("!players").error(f"{e}")
#
#
# @armanator.command(name="all",
#                    description=("Shows information about both" +
#                                 "the server and its players."),
#                    brief="Shows player and server info.",
#                    aliases=["both"],
#                    pass_context=True)
# async def all(ctx):
#     """Shows information about both the server and its players."""
#     try:
#         for i in range(len(config.game_servers)):
#             address = config.game_servers[i]
#             result = await get_info(address, server_info=True, player_info=True, embed=config.embed)
#             if result is None:
#                 raise Exception("get_info returned None")
#
#             if i < len(config.game_servers) - 1:
#                 await send_msg(result, config.bot_channel, True)
#             else:
#                 await send_msg(result, config.bot_channel, False)
#     except Exception as e:
#         logger.getChild("!all").error(f"{e}")


@armanator.command(name="info",
                   description="Shows important community URLs.",
                   brief="Shows important community URLs.",
                   aliases=["group", "community"],
                   pass_context=True)
async def info(ctx):
    """Shows information about the AoU group."""
    try:
        if config.embed:
            embed = Embed(colour=Color.blue())
            embed.add_field(name="**Community links**", value=config.links)
            await config.bot_channel.send(embed=embed)
        else:
            await config.bot_channel.send(config.links)
    except Exception as e:
        logger.getChild("!info").error(f"{e}")


@armanator.command(name="play",
                   description="Shows server connection info.",
                   brief="Shows server connection info.",
                   aliases=["game", "connect"],
                   pass_context=True)
async def play(ctx):
    """Shows information about the AoU servers."""
    try:
        for i in range(len(config.game_servers)):
            address = config.game_servers[i]
            info, host = await retry(get_server_info, address)
            if info is None:
                logger.getChild("!play").error("get_server_info returned None")
                return

            server_name = "**{server_name}**".format(**info)
            server_string, server_url = get_server_url(address)
            server_info = f"Server address: {server_string}\n{server_url}"

            if config.embed:
                embed = Embed(colour=Color.blue())
                embed.add_field(name=server_name, value=server_info)
                await config.bot_channel.send(embed=embed)

            else:
                if i < len(config.game_servers) - 1:
                    server_info += "\n" + config.separator
                await config.bot_channel.send(server_name + "\n" + server_info)
    except Exception as e:
        logger.getChild("!play").error(f"{e}")
        

@armanator.command(name="shutdown",
                   description=("Shutdown the Armanator bot. (Admin only command)"),
                   brief="Shutdown the Armanator bot. (Admin only)",
                   pass_context=True)
async def shutdown(ctx):
    """Turns off the Armanator bot. (Admin only command)"""
    try:
        author = ctx.message.author
        await dialog_yn("shutdown", "Are you sure you want to shutdown the Armanator bot?",
                  author, ctx.message.channel, close, admin_needed=True)
    except Exception as e:
        logger.error(f"(!shutdown) Exception: {e}")


@armanator.command(name="restart",
                   description="Restarts the Armanator bot. (Admin only command)",
                   brief="Restarts the Armanator bot. (Admin only)",
                   pass_context=True)
async def restart(ctx):
    """Restarts the Armanator bot. (Admin only command)"""
    try:
        author = ctx.message.author
        await dialog_yn("restart", "Are you sure you want to restart the Armanator bot?",
                  author, ctx.message.channel, restart_program, admin_needed=True)
    except Exception as e:
        logger.getChild("!restart").error(f"{e}")


@armanator.command(name="update",
                   description=("Updates bot from the repository and restarts the bot." +
                                "(Admin only command)"),
                   brief="Updates bot from the repository. (Admin only)",
                   pass_context=True)
async def update(ctx):
    """Updates the Armanator bot the newest git version. (Admin only command)"""
    try:
        author = ctx.message.author
        await dialog_yn("update", "Are you sure you want to update and restart the Armanator bot?",
                  author, ctx.message.channel, update_program, admin_needed=True)
    except Exception as e:
        logger.getChild("!update").error(f"{e}")


@armanator.command(name="stats",
                   description="Shows game server stats or player stats if player name is supplied.",
                   brief="Shows game server stats or player stats.",
                   pass_context=True)
async def stats(ctx, *args):
    """Shows information about its work - how many users have been promoted
       and how many have been punished."""
    limit, user_name = get_user_limit(args)
    no_stats = True

    try:
        for address in config.game_servers:
            try:
                info, host = await retry(get_server_info, address)
                logger.getChild("stats").info("Stats")

                if info:
                    # general stats with custom limit or without
                    if len(user_name) == 0:
                        result = get_server_stats(db, info["server_name"], limit, config.embed)
                        if result:
                            no_stats = False
                        else:
                            continue
                        if config.embed:
                            await config.bot_channel.send(embed=result)
                        else:
                            await config.bot_channel.send(result)
                    # user stats
                    if len(user_name) > 0:
                        result = get_player_stats(db, info["server_name"], user_name, limit,
                                                  config.embed)
                        if result:
                            no_stats = False
                        else:
                            continue
                        if config.embed:
                            await config.bot_channel.send(embed=result)
                        else:
                            await config.bot_channel.send(result)
            except Exception as e:
                logger.getChild("!stats").error(f"{e}")
        if no_stats:
            await config.bot_channel.send(f"No stats for player: {user_name}")
    except Exception as e:
        pass


armanator.run(config.token)