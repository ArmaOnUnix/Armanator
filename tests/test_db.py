import logging
from config import Config
from sqlalchemy import inspect
import unittest
from database import *

from sqlalchemy import Column, ForeignKey, Integer, String, Float, engine, desc, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import OperationalError


# TODO


class TestConfig(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        logger = logging.getLogger()
        config = Config(logger)
        self.db = Database(config, logger, "test.db")

    def test_load_db(self):
        file_name = "sqlite:///" + self.config.db_file_name
        self.engine = create_engine(file_name)
        inspector = inspect(self.engine)

        tables = {
            "server": ["id", "name"],
            "mission": ["id", "name"],
            "mission_date": ["id", "server_id", "mission_id", "timestamp", "playtime", "player_minute"],
            "player": ["id", "name"],
            "player_mission": ["id", "player_id", "missions_id", "server_id", "score", "playtime"]
        }

        for table_name in inspector.get_table_names():
            cols = inspector.get_columns(table_name)
            for col in tables[table_name]:
                if col not in cols:
                    raise Exception(f"missing column {col}")

    def test_get_player_id(self):
        player_id = self.db.get_player_id({"name": "Ludovit"})
        self.assertEqual(1, player_id, "Should be: 1")

    def test_get_mission_id(self):
        mission_id = self.db.get_mission_id("Best mish")
        self.assertEqual(1, mission_id, "Should be: 1")

    def test_server_id(self):
        server_id = self.db.get_server_id("Da srvr")
        self.assertEqual(1, server_id, "Should be: 1")

    def test_update_mission_date(self):
        mission_id = self.db.get_mission_id("Best mish")
        server_id = self.db.get_server_id("Da srvr")

        no_players = 5
        db_info_refresh = 5

        self.db.update_mission_date(server_id, mission_id, no_players, db_info_refresh)

        timestamp = datetime.utcnow().date().strftime("%y-%m-%d")
        temp_md = self.db.session.query(MissionDate).filter(
            MissionDate.mission_id == mission_id,
            MissionDate.server_id == server_id,
            MissionDate.timestamp == timestamp
        ).first()
        self.assertEqual(temp_md.id, 1, "Should be: 1")
        self.assertEqual(temp_md.server_id, 1, "Should be: 1")
        self.assertEqual(temp_md.mission_id, 1, "Should be: 1")
        self.assertEqual(temp_md.timestamp, timestamp, f"Should be: {timestamp})")
        self.assertEqual(temp_md.playtime, 0, "Should be: 0")
        self.assertEqual(temp_md.player_minute, 0, "Should be: 0")

        self.db.update_mission_date(server_id, mission_id, no_players, db_info_refresh)
        self.db.update_mission_date(server_id, mission_id, no_players, db_info_refresh)
        temp_md = self.db.session.query(MissionDate).filter(
            MissionDate.mission_id == mission_id,
            MissionDate.server_id == server_id,
            MissionDate.timestamp == timestamp
        ).first()
        self.assertEqual(temp_md.id, 1, "Should be: 1")
        self.assertEqual(temp_md.server_id, 1, "Should be: 1")
        self.assertEqual(temp_md.mission_id, 1, "Should be: 1")
        self.assertEqual(temp_md.timestamp, timestamp, f"Should be: {timestamp})")
        self.assertEqual(temp_md.playtime, 2 * db_info_refresh, f"Should be: {2 * db_info_refresh}")
        self.assertEqual(temp_md.player_minute, 2 * db_info_refresh * no_players,
                         f"Should be: {2 * db_info_refresh * no_players}")

    # TODO - test also test_update_plms, test_get_stats a test_get_stats_player
    def test_push_data(self):
        """Tests push_data, update_plms, """
        server_name = "da server"
        mission_name = "da mission"
        players = [{"score": 50, "duration": 180, "name": "Jano"}]
        previous_players = [{"score": 30, "duration": 120, "name": "Jano"}]
        self.db.push_data(server_name, mission_name, len(players), players, previous_players, 180)

        players = [{"score": 60, "duration": 240, "name": "Jano"},
                   {"score": 3, "duration": 38, "name": "Juro"}]
        previous_players = [{"score": 50, "duration": 180, "name": "Jano"}]
        self.db.push_data(server_name, "da mission", len(players), players, previous_players, 180)

        # TODO
        server_stats = self.db.get_stats_server(server_name, limit=1)[0]
        total_sc, missions_sc = self.db.get_stats_player(server_name, "Jano", limit=1)[0]

        self.assertEqual(mission_name, server_stats[0], f"Should be: {mission_name}")
        self.assertEqual(3.0, server_stats[1], "Should be: 3.0")
        self.assertEqual(3.0, server_stats[2], "Should be: 3.0")

if __name__ == "__main__":
    unittest.main()