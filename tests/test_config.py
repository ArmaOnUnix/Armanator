import unittest
import logging
from config import Config

import os


class TestConfig(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.logger = logging.getLogger()
        self.config = Config(self.logger)

    def test_load(self):
        self.config.conf_file = "armanator_test.json"
        self.config.load_conf()

        test_links = """\
**Web page**: webpage
**Discord**: discord
**Steam group**: steam
**Reddit**: reddit
**Email**: email
**Gitlab**: repo"""
        test_servers = [{"host": "test_server1.com", "query_port": 2303, "port": 2302},
                   {"host": "test_server2.com", "query_port": 2323, "port": 2322},
                   {"host": "test_server3.com", "query_port": 2303, "port": 2302,
                    "mods": "ACE,CBA,RHSAFRF,RHSGREF,RHSUSAF,TFAR"}
                   ]

        self.assertEqual(test_links, self.config.links, f"Should be: {test_links}")
        self.assertEqual(3, len(self.config.game_servers), f"Should be: 3")
        for i in range(3):
            server = self.config.game_servers[i]
            test_server = test_servers[i]
            self.assertEqual(test_server["host"], server["host"],
                             f"Should be: {test_server['host']}")
            self.assertEqual(test_server["query_port"], server["query_port"],
                             f"Should be: {test_server['query_port']}")
            self.assertEqual(test_server["port"], server["port"],
                             f"Should be: {test_server['port']}")
            self.assertEqual(test_server["host"], server["host"],
                             f"Should be: {test_server['host']}")

        self.assertEqual("test_server", self.config.server_name, f"Should be: test_server")
        self.assertEqual(387370249323937805, self.config.server_id, f"Should be: 387370249323937805")
        self.assertEqual(387370249323937808, self.config.bot_channel_id,
                         f"Should be: 387370249323937808")
        self.assertEqual(533939161243752066, self.config.server_info_channel_id,
                         f"Should be: 533939161243752066")
        self.assertEqual(60, self.config.server_info_refresh_secs, f"Should be: 60")
        self.assertEqual("Admin", self.config.role_admin_name, f"Should be: Admin")
        self.assertEqual(True, self.config.embed, f"Should be: True")

        self.assertEqual("armanator.db", self.config.db_file_name, f"Should be: armanator.db")
        self.assertEqual(5, self.config.db_info_refresh, f"Should be: 5")

    # not really necessary
    # def test_load_games(self):
    #     pass

    def test_load_tokens(self):
        self.config.token_file = "armanator_token_test.key"
        self.config.load_tokens()
        self.assertEqual("NTI1Mn3iqTY2MjYwODgzzTE2-Dv-oHw.LBQNlnN78hFAVlAkXirOaVyfSx4",
                         self.config.token,
                          "Should be: NTI1Mn3iqTY2MjYwODgzzTE2-Dv-oHw.LBQNlnN78hFAVlAkXirOaVyfSx4")
        self.assertEqual("595380168260884816", self.config.client_id, "Should be: 595380168260884816")


if __name__ == '__main__':
    unittest.main()
