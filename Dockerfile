FROM python:latest

RUN apt-get update; apt-get -yqq upgrade
RUN apt-get install -yqq python3-pip git
#RUN cd /; git clone https://gitlab.com/ArmaOnUnix/Armanator.git

RUN mkdir /armanator
RUN useradd -m armanator
RUN chown -R armanator /armanator
USER armanator
WORKDIR /armanator

RUN pip install --upgrade pip
COPY requirements.txt .
RUN pip install --user -r requirements.txt && rm requirements.txt

VOLUME /armanator

STOPSIGNAL SIGINT
CMD ["python3", "armanator.py"]
