#!/usr/bin/env bash
# you need to manually copy/create in the destination dir armanator_token.key, armanator.json config.
# this script removes the old version of armanator containers and images before it builds a new
# version.
docker stop $(docker ps -a | grep armanator | cut -d' ' -f1)
docker rm $(docker ps -a | grep armanator | cut -d' ' -f1)
docker rmi armanator
docker build -t armanator .
