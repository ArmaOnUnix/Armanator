import discord
import json
import sys


class Config:
    def __init__(self, logger):
        """Set default config values"""
        self.logger = logger.getChild("Config")

        self.retry_count = 3
        self.separator = '------------------------------\n'
        self.embed = True
        # self.conf_file = "armanator.json"
        # self.token_file = "armanator_token.key"
        # Test version
        self.conf_file = "armanator_test.json"
        self.token_file = "armanator_token_test.key"
        self.play_file = "things_to_play_with.txt"
        self.bot_prefix = ("!")

        self.links = ""
        self.game_servers = []
        self.bot_channel_id = ""
        self.bot_channel = discord.channel
        self.server_info_channel_id = ""
        self.server_info_channel = discord.channel

        self.client_id = ""
        self.token = ""

        # Discord related
        self.server_id = ""
        self.server_name = ""
        self.discord_server = discord.Guild
        self.server_info_refresh_secs = 60
        # threshold (absolute value) for recognizing if the drop/raise to 0 score could be caused by
        # client crash
        self.score_threshold = 5
        self.role_admin_name = ""
        self.role_admin = discord.Role

        self.db_file_name = "armanator.db"
        # db (mission stats) data refresh interval in minutes
        self.db_info_refresh = 5

        self.current_image_url = ''
        self.change_game_interval_min = 300
        self.change_game_interval_max = 3600

        # number of times to refresh a server until its declared dead x_x
        self.game_server_refresh_count = 10

    def load_conf(self):
        """Loads bot configuration from CONF_FILE."""
        try:
            with open(self.conf_file) as file:
                data = json.load(file)
        except IOError:
            self.logger.getChild("load_conf").error(f'Opening {self.conf_file} failed!')
            sys.exit(1)
        except json.JSONDecodeError as e:
            self.logger.getChild("load_conf").error(f'JSON Decoding {self.conf_file} failed!\n{e}')
            sys.exit(1)

        self.links = '\n'.join(link for link in data.setdefault('links', []))

        self.game_servers += [server for server in data.setdefault('game_servers', [])]

        # Discord related
        self.server_name = data.setdefault('server_name', self.server_name)
        self.server_id = data.setdefault('server_id', self.server_id)
        self.bot_channel_id = data.setdefault('bot_channel_id', self.bot_channel_id)
        self.server_info_channel_id = data.setdefault('server_info_channel_id',
                                                      self.server_info_channel_id)
        self.server_info_refresh_secs = data.setdefault('server_info_refresh_secs',
                                                        self.server_info_refresh_secs)
        self.role_admin_name = data.setdefault('role_admin', self.role_admin_name)
        self.embed = data.setdefault('embed', self.embed)

        self.db_file_name = data.setdefault('db_filename', self.db_file_name)
        self.db_info_refresh = data.setdefault('db_info_refresh', self.db_info_refresh)

    def load_games(self):
        try:
            with open("./things_to_play_with.txt") as file:
                self.games = file.readlines()
        except IOError:
            self.logger.getChild("load_games").error("Could not open things_to_play_with.txt")
            sys.exit(1)

    def load_tokens(self):
        """Loads bot credentials from TOKEN_FILE."""
        try:
            with open(self.token_file) as file:
                data = json.load(file)
        except IOError:
            self.logger.getChild("load_tokens").error(f'Opening failed! {self.token_file}')
            sys.exit(1)
        except json.JSONDecodeError as e:
            self.logger.getChild("load_tokens").error(f'JSON Decoding {self.token_file} failed!\n{e}')
            sys.exit(1)

        if "client_id" not in data:
            self.logger.getChild("load_tokens").error("Missing bot client_id !")
            sys.exit(1)
        else:
            self.client_id = data["client_id"]
        if "secret" not in data:
            self.logger.getChild("load_tokens").error("Missing bot secret !")
            sys.exit(1)
        else:
            self.token = data["secret"]

        self.logger.getChild("load_tokens").info("Token loaded succesfully!")
