import asyncio
from discord import Color, Embed
import logging
from logging.handlers import RotatingFileHandler
import sys
from valve.source.a2s import NoResponseError, ServerQuerier
from datetime import datetime


def init_logger():
    logger = logging.getLogger("discord")
    logger.setLevel(logging.INFO)
    handler = RotatingFileHandler("armanator.log", maxBytes=5 * 1024 * 1024, backupCount=4,
                                  encoding="utf-8", mode="w")
    handler.setFormatter(logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s"))
    logger.addHandler(handler)
    handler_stream = logging.StreamHandler(sys.stdout)
    handler_stream.setFormatter(
        logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s"))
    logger.addHandler(handler_stream)
    return logger


logger = init_logger()

# TODO create tests for common


async def retry_async(f, *args, retry_count=5):
    try:
        for i in range(retry_count):
            result = await f(*args)
            if result:
                return result
            await asyncio.sleep(1)
            logger.getChild("retry_async").warning(f"Retry async {str(f.__name__)}")
    except Exception as e:
        logger.getChild("retry_async").error(f"{e}")


async def retry(f, *args, retry_count=5):
    try:
        for i in range(retry_count):
            result = f(*args)
            if result:
                return result
            await asyncio.sleep(1)
            logger.getChild("retry").warning(f"Retry {str(f.__name__)}")
    except Exception as e:
        logger.getChild("retry").error(f"{e}")


def get_server_info(address):
    """Returns Valve server info."""
    try:
        with ServerQuerier((address["host"], address["query_port"]), timeout=1) as server:
            result = server.info()
            return result, f'{address["host"]}:{str(address["query_port"])}'
    except Exception as e:
        logger.getChild("get_server_info").error(f"host: {address['host']}:{address['query_port']} "
                                                 f"{e}")
    return None, None


def get_player_info(address):
    """Returns a string containing list of players and scores."""
    try:
        with ServerQuerier((address["host"], address["query_port"])) as server:
            data = ""
            players = server.players()
            caption = "**Score - Player**"
            players = sorted(players["players"], key=lambda p: p["score"], reverse=True)
            for player in players:
                data += "{score} - {name}\n".format(**player)
            if len(data) < 1:
                data = 'None\n'
            return caption, data
    except Exception as e:
        logger.getChild("get_server_info").error(f"{e}")


async def get_info_db(game_servers: list):
    """Retrieves data for storing in the db."""
    try:
        result = []
        # players from previous tick
        get_info_db.players_previous = {}
        for address in game_servers:
            info, host = await retry(get_server_info, address, retry_count=2)
            # save to db only if there are players on the server and a mission was selected
            if info and info["player_count"] > 0 and info["game"] != "Arma 3":
                with ServerQuerier((address["host"], address["query_port"])) as server:
                    players = server.players()
                    players = sorted(players["players"], key=lambda p: p["score"], reverse=True)
                address_key = address["host"] + str(address["query_port"])
                result.append((info["server_name"], info["game"], int(info["player_count"]),
                               players, get_info_db.players_previous.setdefault(address_key, [])))
                get_info_db.players_previous[address_key] = players
        return result
    except Exception as e:
        logger.getChild("get_info_db").error(f"{e}")


async def get_info(address, player_info=False, server_info=False, embed=False,
                   timestamp_info=False):
    """Returns formated output, if embed is True, returns embed, otherwise returns data as string"""
    result = ""
    try:
        info, host = await retry(get_server_info, address)
        if not info or not host:
            logger.getChild("get_info").warning("get_server_info returned None")
            return
        server_name = "**{server_name}**".format(**info)
        address_string, address_url = get_server_url(address)
        server_data = ""
        player_server_data = ""
        caption_players = ""
        timestamp = ""
        players = ""
        if server_info:
            # mods
            mods = ""
            if "mods" in address:
                mods = f"Mods: {address['mods']}\n"
            # other server info
            server_data = (f"Players: {info['player_count']}/{info['max_players']}\n"
                           f"Map: {info['map']}\n"
                           f"Mission: {info['game']}\n"
                           f"Host: [{address_string}]({address_url})\n"
                           f"Run: {address_url}\n"
                           f"{mods}Version: {info['version']}\n\n")
        if player_info:
            caption_players, players = await retry(get_player_info, address)
            player_server_data = ("Players: {player_count}/{max_players}\n".format(**info))
        if timestamp_info:
            update_time = datetime.utcnow().strftime("%H:%M:%S")
            timestamp = f"Last update: {update_time} UTC"

        if embed:
            result = Embed(colour=Color.blue())
            if server_info:
                result.add_field(name=server_name, value=server_data)
            if player_info and not server_info:
                result.add_field(name=server_name, value=player_server_data)
            if player_info:
                result.add_field(name=caption_players, value=players)
            if timestamp_info:
                result.set_footer(text=timestamp)
        else:
            result = ""
            if server_info:
                result += server_name + "\n" + server_data
            if player_info and not server_info:
                result += server_name + "\n" + player_server_data
            if player_info:
                result += caption_players + "\n" + players
            if timestamp_info:
                result += "\n" + timestamp
        return result
    except Exception as e:
        logger.getChild("get_info").error(f"{e}")


def get_offline_msg(address: dict, embed: bool, timestamp_info: bool):
    name = f"**{address['host']}:{address['port']}**"
    value = "Server is offline/unreachable."
    if timestamp_info:
        update_time = datetime.utcnow().strftime("%H:%M:%S")
        timestamp = f"Last update: {update_time} UTC"

    if embed:
        result = Embed(colour=Color.blue())
        result.add_field(name=name, value=value)
        if timestamp_info:
            result.set_footer(text=timestamp)
    else:
        result = f"{name}\n{value}"
        if timestamp_info:
            result += "\n" + timestamp
    return result


def get_server_url(address):
    """Return complete server URL for launching steam game."""
    try:
        host, port = address['host'], address['port']
        address_string = f'{host}:{port}'
        address_url = f"steam://run/107410//-connect={host}%20-port={port}"
        return address_string, address_url
    except Exception as e:
        logger.getChild("get_server_url").error(f"{e}")


def get_user_limit(args: list):
    """Parse user name (and limit) from args."""
    limit = 5
    user_name = ""
    if len(args):
        # last arg is limit
        if args[-1].isdecimal():
            limit = int(args[-1])
            user_name = " ".join(args[0:-1])
        # all args are part of name
        else:
            user_name = " ".join(args[0:])
    return limit, user_name


def get_server_stats(db, server_name, limit: int, embed: bool):
    """Return stats for server from the database."""
    data = db.get_stats_server(server_name, limit)
    logger.getChild("get_server_stats").info(f"Stats {server_name}")

    if embed:
        result = Embed(title="**" + server_name + "**", colour=Color.blue())
        missions = ""
        stats = ""
        # if there is some data in the database
        if len(data) > 0:
            for row in data:
                mission_name = sanitize_none(row[0], str)
                playtime = sanitize_none(row[1], float)
                player_minute = sanitize_none(row[2], float)

                missions += mission_name + "\n"
                # playtime in hours
                stats += f"\t{(playtime / 60):.1f} / {(player_minute):.1f}\n"
            result.add_field(name="**Mission**", value=missions, inline=True)
            result.add_field(name="**Playtime / Avg players**", value=stats, inline=True)
        # if the database is empty
        else:
            result.add_field(name="Nothing to show so far...", value=":poop:", inline=True)
        return result
    # TODO add no embed option ?


def get_player_stats(db, server_name, user_name, limit: int, embed: bool):
    """Return stats for player from the database."""
    total_sc, missions_sc = db.get_stats_player(server_name, user_name, limit)
    if not total_sc or not missions_sc:
        return
    if embed:
        result = Embed(title=f"**Stats for player: {user_name}**", colour=Color.blue())
        # result.add_field(name="**Total score**", value=total_sc[0], inline=True)
        # playtime in hours
        result.add_field(name="**Total playtime [h]**", value=f"{(total_sc[1]):.1f}", inline=True)
        result.add_field(name="-" * 90, value=f"**{server_name}**",
                        inline=False)
        missions = ""
        scores = ""
        playtimes = ""
        for msc in missions_sc:
            missions += f"{msc[0]}\n"
            scores += f"{msc[1]}\n"
            # playtime in hours
            playtimes += f"{(msc[2]):.1f}\n"
        result.add_field(name="**Mission**", value=missions, inline=True)
        # result.add_field(name="**Score**", value=scores, inline=True)
        result.add_field(name="**Playtime [h]**", value=playtimes, inline=True)
        return result
    # TODO add no embed option ?


def sanitize_none(obj, t):
    if obj is None:
        if t is str:
            return 'None'
        elif t is int or t is float:
            return 0
        else:
            return obj
    else:
        return obj


